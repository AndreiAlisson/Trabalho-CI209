     INCDIR = $(PREFIX)/include
     LIBDIR = $(PREFIX)/lib

       CC = gcc -g -Wall
       OBJ = floodit.o

%.o:  %.c $(LIBDIR)/libgrafico.a
	$(CC) -c $(INCS) $<

all: floodit

floodit: $(OBJ)
		$(CC)  -o $@ $^

clean:
	@echo "Limpando sujeira ..."
	@rm -f *% *.bak *~

distclean: clean
	@echo "Limpando tudo ..."
	@rm -rf core *.o floodit

dist:    distclean
	@echo "Gerando arquivo tar ..."
	@rm -f ../floodit.tar.bz2
	@(cd ..; tar -cvzf floodit.tar.bz2 Trabalho-CI209)
