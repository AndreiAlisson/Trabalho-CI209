#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/* Return the f cost given a node */
#define fcost(node) ((node).hcost + (node).gcost)
/* Converts close set's index to the equivalent in the buffer */
#define close_cost(cost, gcost) (cost - gcost)

#define index(c, l, colS) ((c) + (l) * (colS))
#define conv_line(index, size) ((index) / (size))
#define conv_column(index, size) ((index) % (size))

typedef int cost_t;

/* 
	Enumarates the sets used in the a* alg. 
		- None set: Temporary set used when a new node is discovered.
		- Open set: Nodes not explored yet.
		- Close set: Nodes already explored. 
*/
enum NodeSet { noneset = 0, openset = 1, closeset = 2 };

typedef struct List {
	void *data;
	struct List *next;
} List;

typedef struct Group {
	int color;
	short size;
	short wasVisit;
	int bufferIndex;
	List *border;
} Group;

typedef struct State {
	/* Origin is in the array's first position. */
	Group **groups;
	int groupsCounter;
	char alignment[4];
} State;

typedef struct Node {
	State state;
	Group *group;
	cost_t hcost, gcost;
	enum NodeSet set;
	int parentIndex;
} Node;

static struct Problem {
	State initial;
	int size;
	int colorsAmount;
} problem;

typedef struct Problem Problem;

typedef struct Solution {
	int *path;
	int size;
} Solution;

/*static Node buffer[BUFFER_SIZE];*/
/*USED FOR LOG (Will be removed)*/
static FILE *arq;	
static List *ltmp;
static Group *gtmp;
static int iteration = 0;
/******************/

/*Will be removed*/
void Log_Graph(State *state){

	fprintf(arq, "\nLists iteration %d: [Format: Color(Size)]\n", iteration++);
	for (int i = 0; i < state->groupsCounter; i++){
		fprintf(arq, "Node: %d(%d). Border: ", state->groups[i]->color, state->groups[i]->size);
		ltmp = (List*) state->groups[i]->border;
		while(ltmp){
			gtmp = (Group*) ltmp->data;
			fprintf(arq, "%d(%d), ", gtmp->color, gtmp->size);
			ltmp = ltmp->next;
		}
		fprintf(arq, "\n");
	}

}

/*Will be removed*/
void Log_Buffer(Node buffer[]){

	fprintf(arq, "\nBuffer Heuristic: [Format: Color(Size)]\n");
	for (int i = 0; i < problem.initial.groupsCounter; i++){
		fprintf(arq, "Node: %d(%d), parentIndex: %d, gcost: %d, hcost: %d\n", buffer[i].group->color, buffer[i].group->size, buffer[i].parentIndex, buffer[i].gcost, buffer[i].hcost);
	}

}

void Connect_Group(Group *a, Group *b) {
	
	if(a == b)
		return;

	List *tmp = a->border;

	if (tmp != NULL) {
		if (tmp->data == b)
			return;
		while (tmp->next != NULL) {
			tmp = tmp->next;
			if (tmp->data == b)
				return;
		} 
	}

	List *connection_a, *connection_b;

	connection_a = (List*)malloc(sizeof(List));
	connection_b = (List*)malloc(sizeof(List));

	connection_a->data = b;
	connection_a->next = a->border;
	a->border = connection_a;

	connection_b->data = a;
	connection_b->next = b->border;
	b->border = connection_b;
}

/*removes all associations of b with the rm group*/
void Disconnect_Lists(Group *a, Group *b, Group *rm) {

	List *connection_gb = b->border, *connection_tmp = NULL;

	while(connection_gb){
		if(connection_gb->data == rm){
			if(connection_gb->next == NULL && connection_tmp == NULL)
				b->border = NULL;
			else if(connection_gb->next == NULL && connection_tmp != NULL)
				connection_tmp->next = NULL;
			else if(connection_gb == b->border)
				b->border = connection_gb->next;
			else
				connection_tmp->next = connection_gb->next;
			break;
		}
		connection_tmp = connection_gb;
		connection_gb = connection_gb->next;
	}

}

void Get_Problem() {
	int linS, colS;

	scanf("%d", &linS);
	scanf("%d", &colS);
	scanf("%d", &problem.colorsAmount);

	int *tmp, *colorStack, *borderQueue;
	Group **groups;
	int groupsCounter = 1;

	tmp = (int*)malloc(linS * colS * sizeof(int));
	colorStack = (int*)malloc(linS * colS * sizeof(int));
	borderQueue = (int*)malloc(linS * colS * sizeof(int) * 8);
	groups = (Group**)malloc(linS * colS * sizeof(Group*));

	for (int l = 0; l < linS; l++)
		for (int c = 0; c < colS; c++)
			scanf("%d", &(tmp[index(c , l, colS)]));

	State state;
	Group *group = (Group*)malloc(sizeof(Group));
	int borderQueueSize = 1;
	int colorStackStart, colorStackEnd;

	int color = 0, c = 0, l = 0;

	groups[0] = group;

	borderQueue[0] = 0;
	while (borderQueueSize > 0) {
		while ((color = tmp[borderQueue[borderQueueSize - 1]]) < 0)
			if (--borderQueueSize == 0) break;
		if(borderQueueSize == 0) break;
		
		group->color = color;
		group->size = 1;
		group->border = NULL;

		colorStackEnd = 1, colorStackStart = 0;
		colorStack[0] = borderQueue[borderQueueSize - 1];

		/* Dequeue from border queue */
		borderQueueSize--;

		while (colorStackStart < colorStackEnd) {
			c = conv_column(colorStack[colorStackStart], colS);
			l = conv_line(colorStack[colorStackStart], colS);

			tmp[colorStack[colorStackStart]] = -groupsCounter;

			/* Pop from color stack */
			colorStackStart++;

			/* Vertical and Horizontal */
			if (c - 1 >= 0) {
				if (tmp[index(c - 1, l, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c - 1, l, colS), tmp[index(c - 1, l, colS)] = -groupsCounter;
				else if (tmp[index(c - 1, l, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c - 1, l, colS);
			}
			if (c + 1 < colS) {
				if (tmp[index(c + 1, l, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c + 1, l, colS), tmp[index(c + 1, l, colS)] = -groupsCounter;
				else if (tmp[index(c + 1, l, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c + 1, l, colS);
			}
			if (l - 1 >= 0) {
				if (tmp[index(c, l - 1, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c, l - 1, colS), tmp[index(c, l - 1, colS)] = -groupsCounter;
				else if (tmp[index(c, l - 1, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c, l - 1, colS);
			}
			if (l + 1 < linS) {
				if (tmp[index(c, l + 1, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c, l + 1, colS), tmp[index(c, l + 1, colS)] = -groupsCounter;
				else if (tmp[index(c, l + 1, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c, l + 1, colS);
			}

			/* Diagonal */
			if (c - 1 >= 0 && l - 1 >= 0) {
				if (tmp[index(c - 1, l - 1, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c-1, l-1, colS), tmp[index(c - 1, l - 1, colS)] = -groupsCounter;
				else if (tmp[index(c - 1, l - 1, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c - 1, l - 1, colS);
			}
			if (c - 1 >= 0 && l + 1 < linS) {
				if (tmp[index(c - 1, l + 1, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c-1, l+1, colS), tmp[index(c - 1, l + 1, colS)] = -groupsCounter;
				else if (tmp[index(c - 1, l + 1, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c - 1, l + 1, colS);
			}
			if (c + 1 < colS && l - 1 >= 0) {
				if (tmp[index(c + 1, l - 1, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c+1, l-1, colS), tmp[index(c + 1, l - 1, colS)] = -groupsCounter;
				else if (tmp[index(c + 1, l - 1, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c + 1, l - 1, colS);
			}
			if (c + 1 < colS && l + 1 < linS) {
				if (tmp[index(c + 1, l + 1, colS)] == color)
					group->size++, colorStack[colorStackEnd++] = index(c+1, l+1, colS), tmp[index(c + 1, l + 1, colS)] = -groupsCounter;
				else if (tmp[index(c + 1, l + 1, colS)] > 0)
					borderQueue[borderQueueSize++] = index(c + 1, l + 1, colS);
			}
		}

		group = (Group*)malloc(sizeof(Group));
		groups[groupsCounter] = group;
		groupsCounter++;
	}

	groupsCounter--;

	state.groups = (Group**)malloc(sizeof(Group*) * groupsCounter);
	for (int i = 0; i < groupsCounter; i++)
		state.groups[i] = groups[i];
	state.groupsCounter = groupsCounter;
	problem.initial = state;

	for (int l = 0; l < linS - 1; l++) {
		for (int c = 0; c < colS; c++) {
			group = groups[-tmp[index(c, l, colS)] - 1];
			color = group->color;

			/* Isn't necessary check the entire neighborhood */
			if (c + 1 < colS) {
				if (color != groups[-tmp[index(c + 1, l, colS)] - 1]->color)
					Connect_Group(group, groups[-tmp[index(c + 1, l, colS)] - 1]);
				if (l + 1 < linS)
					if (color != groups[-tmp[index(c + 1, l + 1, colS)] - 1]->color)
						Connect_Group(group, groups[-tmp[index(c + 1, l + 1, colS)] - 1]);
			}
			if (l+1 < linS && color != groups[-tmp[index(c, l+1, colS)]-1]->color)
				Connect_Group(group, groups[-tmp[index(c, l + 1, colS)] - 1]);
			if (c - 1 >= 0 && l + 1 < linS)
				if (color != groups[-tmp[index(c - 1, l + 1, colS)] - 1]->color)
					Connect_Group(group, groups[-tmp[index(c - 1, l + 1, colS)] - 1]);
		}
	}

	/*Will be removed*/
	Log_Graph(&state);

	free(tmp);
	free(colorStack);
	free(groups);
	free(borderQueue);
}

/* Return the heuristic value for the given state */
void Heuristic(Node *buffer) {

	int count = 0, current = 0;
	/*cost is the distance of the root and the further leaf */
	cost_t cost = -1;

	buffer[count++].group = problem.initial.groups[0];
	buffer[current].parentIndex = 0;
	buffer[current].gcost = -1;
	buffer[current].group->wasVisit = 1;

	List *l_tmp;
	Group *g_tmp;

	/*Put the graph into the buffer vector and calculates the gcost value for all nodes*/
	while(current < problem.initial.groupsCounter){

		l_tmp = (List*) buffer[current].group->border;

		cost = buffer[buffer[current].parentIndex].gcost+1;
		buffer[current].gcost = cost;
		buffer[current].hcost = 0;

		while(l_tmp){
			g_tmp = (Group*) l_tmp->data;

			if(!g_tmp->wasVisit){
				buffer[count].group = g_tmp;
				buffer[count].parentIndex = current;
				buffer[count].group->bufferIndex = count;
				count++;
				g_tmp->wasVisit = 1;
			}
			l_tmp = l_tmp->next;
		}
		current++;
	}

	current--;

	/*Calculares the hcost for all nodes as the distance between the node and the farthest leaf*/
	while(current > 0){
		buffer[current].hcost = close_cost(cost, buffer[current].gcost);
		
		if(buffer[buffer[current].parentIndex].hcost < buffer[current].hcost+1)
			buffer[buffer[current].parentIndex].hcost = buffer[current].hcost+1;

		current--;
	}

	buffer[current].hcost = cost;

}

/* Return true if the state given is a final state */
bool Is_final_state() {
	int color = problem.initial.groups[0]->color;

	for (int i = 1; i < problem.initial.groupsCounter; i++)
		if (problem.initial.groups[i]->color != color)
			return false;

	return true;
}

int Paint(State *base, int color) {
	Node node;
	int before = base->groups[0]->color;
	List *interator = base->groups[0]->border;
	Group *group;
	Group **join = (Group**)malloc(sizeof(Group*) * base->groupsCounter);
	int joinCounter = 0;

	base->groups[0]->color = color;
	while (interator) {
		group = (Group*)interator->data;

		if (group->color == color) {
			join[joinCounter++] = group;
			group->wasVisit = true;
		}

		interator = interator->next;
	}

	node.state.groupsCounter = base->groupsCounter - joinCounter;
	node.state.groups = (Group**)malloc(sizeof(Group*) * node.state.groupsCounter);
	node.state.groups[0] = (Group*)malloc(sizeof(Group));
	node.state.groups[0]->color = color;
	node.state.groups[0]->size = base->groups[0]->size;
	node.state.groups[0]->border = base->groups[0]->border; 

	int counter = 1;
	for (int i = 1; i < base->groupsCounter; i++) {
		/* Join group */
		if (base->groups[i]->wasVisit) {
			node.state.groups[0]->size += base->groups[i]->size;

			/* WARNING */
			interator = base->groups[i]->border;
			while (interator) {
				group = (Group*)interator->data;

				/*isn't consistent yet, need to see the disconnect function*/
				Disconnect_Lists(node.state.groups[0], group, base->groups[i]);
				Connect_Group(node.state.groups[0], group);

				interator = interator->next;
			}
			/*************/

			free(base->groups[i]->border);

			base->groups[i]->wasVisit = false;
		}	
		/* Copy group */
		else 
			node.state.groups[counter++] = base->groups[i];

	}

	free(join);

	problem.initial = node.state;

	/*Will be removed*/
	/*Log_Graph(&node.state);*/

	return 0;
}

/* 
	Explore node's neighbours.
		node: Node that will have his neighbours discover.
		indexesBuffer: Where will be store the neighbours's indexes.
		size: Buffer's size.
*/
int Branch(Node *buffer) {

	int index, color, cost = -1;
	List *l_tmp = (List*) buffer[0].group->border;
	Group *g_tmp;

	while(l_tmp){
		g_tmp = (Group*) l_tmp->data;
		if(buffer[g_tmp->bufferIndex].hcost > cost){
			index = g_tmp->bufferIndex;
			cost = buffer[g_tmp->bufferIndex].hcost;
		}
		l_tmp = l_tmp->next;
	}

	color = Paint(&problem.initial, buffer[index].group->color);

	return color;
	
}

/* Reverse an array */
void Reverse(int *array, int size) {
	for (int i = 0; i < (size/2); i++) {
		int tmp = array[i];
		array[i] = array[size - i - 1];
		array[size - i - 1] = tmp;	
	}
}

/* Create the search result path */
Solution* Make_Path(Node buffer[], int final_node) {

	int *a = malloc(sizeof(int)), count = 0;

	if(buffer[final_node].group == problem.initial.groups[0])
		a[count++] = final_node;

	while(buffer[final_node].group != problem.initial.groups[0]){
		a = realloc(a, (count+1) * sizeof(int));
		a[count++] = final_node;
		final_node = buffer[final_node].parentIndex;
	}

	Reverse(a, count);

	Solution *solution = malloc(sizeof(Solution));
	solution->path = malloc(count * sizeof(int));
	solution->size = count;

	return solution;
}

/* Execute a* search alg. */
Solution* Execute() {

	int color;
	/*temporary vector*/
	int *a = malloc(sizeof(int)), count = 0;;

	Node buffer[problem.initial.groupsCounter];

	/*temporary*/
	while(!Is_final_state()){
		Heuristic(buffer);
		/*Will be removed*/
		Log_Buffer(buffer);

		color = Branch(buffer);

		a = realloc(a, (count+1) * sizeof(int));
		a[count++] = color;
	}

	Solution *solution = malloc(sizeof(Solution));
	solution->path = malloc(count * sizeof(int));
	solution->size = count;
	/*********************************/

	return solution;

}

void Print_Solution(Solution *solution) {

	printf("%d\n", solution->size);
	for (int i = 0; i < solution->size; i++)
		printf("%d ", solution->path[i]);
	printf("\n");

}

void Free_States() {

}

int main() {
	arq = fopen("log.txt", "w");

	Get_Problem();

	/*Will be removed*/
	/*Paint(&problem.initial, 1);
	if(Is_final_state(&problem.initial))
		fprintf(arq, "\nSucessful!\n");
	Paint(&problem.initial, 2);
	if(Is_final_state(&problem.initial))
		fprintf(arq, "\nSucessful!\n");
	Paint(&problem.initial, 3);
	if(Is_final_state(&problem.initial))
		fprintf(arq, "\nSucessful!\n");*/
	/**************************************/
	
	Solution *solution;

	solution = Execute();
	/*Print_Solution(solution);

	free(problem);
	free(solution);
	Free_States();
	*/
	fclose(arq);

	return 0;
}